//#include<windows.h>
#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif
#include<math.h>
#include <stdlib.h>

void Draw() {
	glClear(GL_COLOR_BUFFER_BIT);
	glColor3d(1,0,0);
	glBegin(GL_TRIANGLE_FAN);
	//glColor3f(0.67f, 0.871f, 0.93f);//cABELLO
	// glColor3f(0.309803921569, 0.372549019608, 0.611764705882);// Azul obcuro ropa
	glColor3f(1.0f, 0.94f, 0.87f);//PIEL
	glVertex2f(-0.32236, -4.5245);//pierna i
	glVertex2f(0.15229, -4.53024);
	glVertex2f(0.16964, -4.95962);
	glVertex2f(-0.2897, -4.94089);
	glEnd();
	
	glBegin(GL_TRIANGLE_FAN);// pierna d
	glVertex2f(0.60023, -4.54083);
	glVertex2f(1.12276, -4.51634);
	glVertex2f(1.03295, -4.93273);
	glVertex2f(0.5839, -4.89191);
	glEnd();

	glBegin(GL_TRIANGLE_FAN);
	glColor3f(0.95, 0.97, 0.99);//pierna i blanco
	glVertex2f(-0.2897, -4.94089);
  glVertex2f(0.16964, -4.95962);
	glVertex2f(0.18113, -5.15172);
	glVertex2f(0.0777, -5.24298);
	glVertex2f(-0.07742, -5.25114);
	glVertex2f(-0.25704, -5.16133);
	glEnd();
	
	glBegin(GL_TRIANGLE_FAN);//pierna d blanco
	glVertex2f(0.5839, -4.89191);
	glVertex2f(1.03295, -4.93273);
	glVertex2f(0.99804, -5.15046); //r
	glVertex2f(0.82455, -5.23286);
	glVertex2f(0.69443, -5.21118);
	glVertex2f(0.54263, -5.16781); //o
	glEnd();
	
	glBegin(GL_TRIANGLE_FAN);
	glColor3f(0.996, 0.843, 0.69);// amarillo i
	glVertex2f(0.99804, -5.15046);
	glVertex2f(0.82455, -5.23286);
	glVertex2f(0.69443, -5.21118);
	glVertex2f(0.54263, -5.16781);
	glVertex2f(0.54697, -5.37165);
	glVertex2f(0.69443, -5.45406);
	glVertex2f(0.8766, -5.47141);
	glVertex2f(1, -5.4);
	glEnd();

	glBegin(GL_TRIANGLE_FAN);//amarillo d
	glVertex2f(0.18113, -5.15172);
	glVertex2f(0.0777, -5.24298);
	glVertex2f(-0.07742, -5.25114);
	glVertex2f(-0.25704, -5.16133);
	glVertex2f(-0.25541, -5.34563);
	glVertex2f(-0.09927, -5.44539);
	glVertex2f(0.08289, -5.45406);
	glVertex2f(0.21301, -5.33262);
	glEnd();

	glBegin(GL_TRIANGLE_FAN); // azul pierna d
	glColor3f(0.309803921569, 0.372549019608, 0.611764705882);// Azul obcuro ropa
	glVertex2f(0.64239, -5.85308);
	glVertex2f(0.54697, -5.37165);
	glVertex2f(0.69443, -5.45406);
	glVertex2f(0.8766, -5.47141);
	glVertex2f(1, -5.4);
	glVertex2f(0.91129, -5.86175);
	glVertex2f(0.96334, -5.97018);
	glVertex2f(0.78985, -6.06994);
	glVertex2f(0.6, -6);
	glEnd();

	glBegin(GL_TRIANGLE_FAN); // azul pierna i
  glVertex2f(-0.15131, -5.87043);
	glVertex2f(-0.2, -6);
	glVertex2f(-0.00385, -6.08295);
	glVertex2f(0.14361, -5.98753);
	glVertex2f(0.09591, -5.87043);
	glVertex2f(0.08289, -5.45406);
	glVertex2f(-0.09927, -5.44539);
	glVertex2f(-0.25541, -5.34563);
	// 	// 
	glEnd();

	glBegin(GL_TRIANGLE_FAN); // falda inferior azul
  glVertex2f(-0.32236, -4.5245);
	glVertex2f(-0.6521, -4.42287);
	glVertex2f(-1.03292, -4.17322);
	glVertex2f(-0.94829, -3.99973);
	glVertex2f(-0.52516, -4.24092);
	glVertex2f(-0.24165, -4.28747);
	glVertex2f(0.18995, -4.30862);
	glVertex2f(0.81619, -4.32132);
	glVertex2f(1.34088, -4.28324);
	glVertex2f(1.44667, -4.45249);
	glVertex2f(1.12276, -4.51634);
	glVertex2f(0.60023, -4.54083);
	glEnd();
	glBegin(GL_TRIANGLE_FAN);
	glVertex2f(1.34088, -4.28324);
	glVertex2f(1.12276, -4.51634);
	glVertex2f(1.44667, -4.45249);
	glVertex2f(1.71748, -4.16899);
	glVertex2f(1.63285, -4.10129);
	
	glEnd();


	glBegin(GL_TRIANGLE_FAN);
	glColor3f(0.996, 0.843, 0.69);// amarillo falda linea
  glVertex2f(1.25134, -4.22073);
	glVertex2f(1.6, -4);
	glVertex2f(1.63285, -4.10129);
	glVertex2f(1.34088, -4.28324);
	glVertex2f(0.81619, -4.32132);
	glVertex2f(0.18995, -4.30862);
	glVertex2f(-0.24165, -4.28747);
	glVertex2f(-0.23771, -4.18604);
	glEnd();
	glBegin(GL_TRIANGLE_FAN);
  glVertex2f(-0.63532, -4.10865);
	glVertex2f(-0.23771, -4.18604);
	glVertex2f(-0.24165, -4.28747);
	glVertex2f(-0.52516, -4.24092);
	glVertex2f(-0.94829, -3.99973);
	glVertex2f(-0.89684, -3.91918);
	glEnd();

	glBegin(GL_TRIANGLE_FAN);
	glColor3f(0.309803921569, 0.372549019608, 0.611764705882);// Azul obcuro ropa
  glVertex2f(-0.63532, -4.10865);
	glVertex2f(-0.94829, -3.99973);
	glVertex2f(-0.69937, -3.76975);
	glVertex2f(-0.45653, -3.67635);
	glVertex2f(-0.25372, -3.80711);
	glVertex2f(-0.06692, -3.9432);
	glVertex2f(-0.23771, -4.18604);
	glEnd();
	glBegin(GL_TRIANGLE_FAN);
	glVertex2f(0.8, -4.2);
	glVertex2f(1.25134, -4.22073);
	glVertex2f(1.6, -4);
	glVertex2f(1.50752, -3.87382);
	glVertex2f(1.39811, -3.91118);
	glVertex2f(1.39811, -3.7564);
	glVertex2f(1.31538, -3.70303);
	glVertex2f(0.86707, -3.96188);
	glVertex2f(0.4401, -3.64432);
	glVertex2f(-0.06692, -3.9432);
	glVertex2f(-0.23771, -4.18604);
	// glVertex2f;
	// glVertex2f;
	glEnd();

	glBegin(GL_TRIANGLE_FAN);
	glColor3f(0.95, 0.97, 0.99);//blanco contorno vestido
  glVertex2f(0.86707, -3.96188);
	glVertex2f(1.31538, -3.70303);
  glVertex2f(1.15935, -3.67985);
	glVertex2f(1.12306, -3.59446);
	glVertex2f(0.9096, -3.77164);
  glVertex2f(0.5275, -3.47919);
	glVertex2f(0.4401, -3.64432);
  // glVertex2f;
	// glVertex2f;
	glEnd();

	glBegin(GL_TRIANGLE_FAN);
  
  glVertex2f(-0.06692, -3.9432);
	glVertex2f(0.4401, -3.64432);
	glVertex2f(0.5275, -3.47919);
	glVertex2f(0.33538, -3.48773);
	glVertex2f(-0.04672, -3.72895);
	glVertex2f(-0.32636, -3.51335);
	glVertex2f(-0.33277, -3.6521);
	glVertex2f(-0.45653, -3.67635);
	glVertex2f(-0.25372, -3.80711);
	glEnd();

	glBegin(GL_TRIANGLE_FAN);
  glVertex2f(0.45382, -3.12531);
	glVertex2f(0.5275, -3.47919);
	glVertex2f(0.33538, -3.48773);
	glVertex2f(0.35672, -3.15473);
	glVertex2f(0.4, -3.1);
	glVertex2f(0.40743, -2.91569);
	glVertex2f(0.47787, -2.91053);
	glVertex2f(0.5, -3.1);
	glVertex2f(0.5339, -3.16327);
	glEnd();

	glBegin(GL_TRIANGLE_FAN); // bubi d
	glColor3f(0.309803921569, 0.372549019608, 0.611764705882);// Azul obcuro ropa
  glVertex2f(1.12068, -3.20252);
  
	glVertex2f(1.06688, -2.36085);
	glVertex2f(1.22828, -2.42159);
	glVertex2f(1.3, -2.5);
	glVertex2f(1.29075, -2.63852);
	
	glVertex2f(1.34073, -2.7855);
	glVertex2f(1.32737, -2.70724);
	glVertex2f(1.32025, -2.87627);
	glVertex2f(1.29769, -2.96477);
	glVertex2f(1.22133, -3.06543);
	
	glVertex2f(1.1658, -3.15393);
	glVertex2f(1.17621, -3.23897);
	glVertex2f(1.25257, -3.40383);
	glVertex2f(1.1363, -3.45937);
	
	glVertex2f(1.12306, -3.59446);
	glVertex2f(0.9096, -3.77164);
	glVertex2f(0.5275, -3.47919);
	glVertex2f(0.69377, -3.26153);
	glVertex2f(0.92805, -3.09146);
	glVertex2f(0.92979, -2.93354);
	glVertex2f(0.9, -2.8);
	glVertex2f(0.94887, -2.74958);
	glVertex2f(1.05473, -2.79991);
	glVertex2f(1.06688, -2.36085);
 	glEnd();
	
	glBegin(GL_TRIANGLES); // bubi d
  glVertex2f(0.5339, -3.16327);
	glVertex2f(0.5275, -3.47919);
	glVertex2f(0.69377, -3.26153);
	glVertex2f(1.22828, -2.42159);
	glVertex2f(1.23175, -2.31226);
	glVertex2f(1.06688, -2.36085);
	glVertex2f(1.29943, -1.97906);
	glVertex2f(1.45735, -1.99468);
	glVertex2f(1.36537, -2.08145);
	glEnd();

	glBegin(GL_TRIANGLE_FAN); // bubi d
	glVertex2f(1.34802, -2.2064);
	glVertex2f(1.34802, -2.2064);
	glVertex2f(1.23175, -2.31226);
	glVertex2f(1.06688, -2.36085);
	glVertex2f(1.36537, -2.08145);
	glVertex2f(1.40529, -2.02592);
	glVertex2f(1.45735, -1.99468);
	glVertex2f(1.43826, -2.07971);
	glEnd();
	glBegin(GL_TRIANGLE_FAN); // bubi I
	glVertex2f(-0.15016, -2.77023);
	glVertex2f(-0.1, -2.6);
	glVertex2f(-0.08526, -2.27581);
	glVertex2f(-0.2, -2.3);
	glVertex2f(-0.29524, -2.36363);
	glVertex2f(-0.38496, -2.45144);
	glVertex2f(-0.45369, -2.61752);
	glVertex2f(-0.4785, -2.73778);
	glVertex2f(-0.45559, -2.89241);
	glVertex2f(-0.4, -3);
	glVertex2f(-0.31242, -3.1253);
	glVertex2f(-0.1368, -3.17111);
	glVertex2f(0.03119, -3.06421);
	glVertex2f(0.02546, -2.81605);
	// glVertex2f;
	glEnd();
	
	glBegin(GL_TRIANGLE_FAN); 
	glVertex2f(-0.04672, -3.72895);
	glVertex2f(0.33538, -3.48773);
	glVertex2f(0.35672, -3.15473);
	glVertex2f(0.18581, -3.23602);
	glVertex2f(0.11327, -3.13103);
	glVertex2f(0.09418, -3.06039);
	glVertex2f(-0.1368, -3.17111);
	glVertex2f(-0.31242, -3.1253);
	glVertex2f(-0.33915, -3.3181);
	glVertex2f(-0.32636, -3.51335);
	glEnd();
	glBegin(GL_TRIANGLE_FAN); 
	glVertex2f(-0.3716, -1.97993);
	glVertex2f(-0.08526, -2.27581);
	glVertex2f(-0.2, -2.3);
	glVertex2f(-0.48996, -1.94747);
	glVertex2f(-0.30097, -1.92648);
	glEnd();


	glBegin(GL_TRIANGLE_FAN); 
	glColor3f(0.4705, 0.7804, 0.7059); // moño 
  glVertex2f(0.18727, -2.7699);
	glVertex2f(0.3477, -3.03471);
	glVertex2f(0.35672, -3.15473);
	glVertex2f(0.18581, -3.23602);
	glVertex2f(0.2, -3.1);
	glVertex2f(0.11327, -3.13103);
	glVertex2f(0.15535, -2.98828);
	glVertex2f(0.09418, -3.06039);
	glVertex2f(0.03119, -3.06421);
	glVertex2f(0.02546, -2.81605);
	glVertex2f(-0.15016, -2.77023);
	glVertex2f(-0.1, -2.6);
	glVertex2f(0.08637, -2.52266);
	glVertex2f(0.25086, -2.50277);
	glVertex2f(0.30525, -2.52399);
	glVertex2f(0.29463, -2.65797);
	glVertex2f(0.33576, -2.74685);
	glVertex2f(0.40743, -2.91569);
	glVertex2f(0.3477, -3.03471);
	// glVertex2f;
	// glVertex2f;
	// glVertex2f;

	glEnd();
	glBegin(GL_TRIANGLE_FAN); 
	glVertex2f(0.2, -3.1);
	glVertex2f(0.3477, -3.03471);
	glVertex2f(0.35672, -3.15473);
	glVertex2f(0.18581, -3.23602);
	glVertex2f(0.11327, -3.13103);
	glVertex2f(0.09418, -3.06039);
	glEnd();
	glBegin(GL_TRIANGLE_FAN); 
	glVertex2f(0.08637, -2.52266);
	glVertex2f(0.25086, -2.50277);
	glVertex2f(0.30525, -2.52399);
	glVertex2f(0.29463, -2.65797);
	glVertex2f(0.02546, -2.81605);
	glVertex2f(-0.15016, -2.77023);
	glVertex2f(-0.1, -2.6);
	glEnd();
	glBegin(GL_TRIANGLE_FAN); 
	glVertex2f(0.7, -2.8);
	glVertex2f(0.56923, -2.71236);
	glVertex2f(0.58913, -2.62746);
	glVertex2f(0.57055, -2.50807);
	glVertex2f(0.83719, -2.53195);
	glVertex2f(0.98974, -2.62215);
	glVertex2f(0.94887, -2.74958);
	glVertex2f(0.9, -2.8);
	glVertex2f(0.92979, -2.93354);
	glVertex2f(0.92805, -3.09146);
	glVertex2f(0.8569, -3.09666);
	glVertex2f(0.81198, -3.05461);
	glVertex2f(0.69377, -3.26153);
	glVertex2f(0.5339, -3.16327);
	glVertex2f(0.5, -3.1);
	glVertex2f(0.54402, -3.02807);
	glVertex2f(0.47787, -2.91053);
	glVertex2f(0.483, -2.80256);
	glVertex2f(0.56923, -2.71236);
	glEnd();

	glBegin(GL_TRIANGLE_FAN); // perla
	glColor3f(0.4431,0.6588,0.84313);
	glVertex2f(0.43923, -2.63409);
	glVertex2f(0.37953, -2.45368);
	glVertex2f(0.48168, -2.44705);
	glVertex2f(0.57055, -2.50807);
	glVertex2f(0.58913, -2.62746);
	glVertex2f(0.56923, -2.71236);
	glVertex2f(0.483, -2.80256);
	glVertex2f(0.3928, -2.78664);
	glVertex2f(0.33576, -2.74685);
	glVertex2f(0.29463, -2.65797);
	glVertex2f(0.30525, -2.52399);
	glVertex2f(0.37953, -2.45368);
	glEnd();
	
	glBegin(GL_TRIANGLE_FAN);//cuello balnco
	glColor3f(0.95, 0.97, 0.99);
	glVertex2f(-0.14376, -2.019);
	glVertex2f(-0.0454, -2.09656);
	glVertex2f(-0.3716, -1.97993);
	glVertex2f(-0.30097, -1.92648);
	glVertex2f(-0.01513, -1.92442);
	// glVertex2f;
	// glVertex2f;
	// glVertex2f;
	glEnd();
	glBegin(GL_TRIANGLES); 
	glVertex2f(1.07638, -2.53733);
	glVertex2f(0.98974, -2.62215);
	glVertex2f(1.05473, -2.79991);
	glEnd();

	glBegin(GL_TRIANGLE_FAN);
	glVertex2f(-0.3716, -1.97993);
	glVertex2f(-0.14376, -2.019);
	glVertex2f(-0.0454, -2.09656);
	glVertex2f(-0.01513, -2.23655);
	glVertex2f(-0.08526, -2.27581);
	glEnd();
	glBegin(GL_TRIANGLE_FAN);
	glVertex2f(0.1, -2.3);
	glVertex2f(0.20242, -2.21763);
	glVertex2f(0.23836, -2.3387);
	glVertex2f(0.37953, -2.45368);
	glVertex2f(0.30525, -2.52399);
	glVertex2f(0.25086, -2.50277);
	glVertex2f(0.08637, -2.52266);
	glVertex2f(-0.1, -2.6);
	glVertex2f(-0.08526, -2.27581);
	glVertex2f(-0.01513, -2.23655);
	glEnd();
	
	glBegin(GL_TRIANGLE_FAN); 
	glVertex2f(0.42942, -2.35762);
	glVertex2f(0.48995, -2.25357);
	glVertex2f(0.61102, -2.37464);
	glVertex2f(0.48168, -2.44705);
	glVertex2f(0.37953, -2.45368);
	glVertex2f(0.23836, -2.3387);
	glEnd();

	glBegin(GL_TRIANGLE_FAN); 
	glVertex2f(0.821, -2.35951);
	glVertex2f(0.61102, -2.37464);
	glVertex2f(0.57055, -2.50807);
	glVertex2f(0.83719, -2.53195);
	glVertex2f(0.98974, -2.62215);
	glVertex2f(1.07638, -2.53733);
	glVertex2f(1.06688, -2.36085);
	glVertex2f(0.9345, -2.32735);
	glVertex2f(0.83424, -2.25547);
	glEnd();
	glBegin(GL_TRIANGLE_FAN); 
	glVertex2f(1.36537, -2.08145);
	glVertex2f(1.40529, -2.02592);
	glVertex2f(1.29943, -1.97906);
	glVertex2f(1.12556, -1.96225);
	glVertex2f(1.12178, -2.08521);
	glVertex2f(1.04422, -2.1344);
	glVertex2f(1.1, -2.2);
	glVertex2f(0.9345, -2.32735);
	glVertex2f(1.06688, -2.36085);
	glEnd();
	
		

	glBegin(GL_TRIANGLE_FAN);
	glColor3f(0.309803921569, 0.372549019608, 0.611764705882);// Azul obcuro ropa
	glVertex2f(0.32916, -2.03603);
	glVertex2f(0.26673, -2.02279);
	glVertex2f(0.20242, -2.21763);
	glVertex2f(0.23836, -2.3387);
	glVertex2f(0.42942, -2.35762);
	glVertex2f(0.48995, -2.25357);
	glVertex2f(0.58075, -2.02657);
	glVertex2f(0.56373, -1.91307);
	glVertex2f(0.3424, -1.92442);
	
	// glVertex2f;
	// glVertex2f;
	// glVertex2f;

	glEnd();
	
	glBegin(GL_TRIANGLE_FAN); 
	glVertex2f(0.83235, -2.14953);
	glVertex2f(0.76046, -1.80335);
	glVertex2f(1.40529, -2.02592);
	glVertex2f(1.35496, -2.14913);
	glVertex2f(1.04422, -2.1344);
	glVertex2f(1.1, -2.2);
	glVertex2f(0.9345, -2.32735);
	glVertex2f(0.83424, -2.25547);
	glVertex2f(0.821, -2.35951);
	glVertex2f(0.61102, -2.37464);
	glVertex2f(0.48995, -2.25357);
	glVertex2f(0.58075, -2.02657);
	glVertex2f(0.72452, -2.0209);
	glVertex2f(0.76046, -1.80335);

	glEnd();

	glBegin(GL_TRIANGLE_FAN); 
	glVertex2f(-0.01513, -1.92442);
	glVertex2f(0.26295, -1.82983);
	glVertex2f(0.20242, -2.21763);
	glVertex2f(0.1, -2.3);
	glVertex2f(-0.01513, -2.23655);
	glVertex2f(-0.0454, -2.09656);
	glVertex2f(-0.14376, -2.019);
	glEnd();
	glBegin(GL_TRIANGLE_FAN);
	glVertex2f(0.20242, -2.21763);
	glVertex2f(0.1, -2.3);
	glVertex2f(0.26673, -2.02279);
	glVertex2f(0.32916, -2.03603);
	glVertex2f(0.3424, -1.92442);
	glEnd();



	glBegin(GL_TRIANGLE_FAN);
	//glColor3f(0.67f, 0.871f, 0.93f);//cABELLO
	// glColor3f(0.309803921569, 0.372549019608, 0.611764705882);// Azul obcuro ropa
	glColor3f(1.0f, 0.94f, 0.87f);//PIEL
	glVertex2f(-0.46281, -2.29135);//Brazo i
	glVertex2f(-0.43,-2.03);
	glVertex2f(-0.2, -2.3);
	glVertex2f(-0.29524, -2.36363);
	glVertex2f(-0.38496, -2.45144);
	glVertex2f(-0.54092, -2.60575);
	glVertex2f(-0.73034, -2.27768);
	glVertex2f(-0.43,-2.03);
	glEnd();
	glBegin(GL_TRIANGLE_FAN); 
	glVertex2f(-0.45653, -3.67635);
	glVertex2f(-0.33277, -3.6521);
	glVertex2f(-0.32636, -3.51335);
	glVertex2f(-0.48859, -3.3191);
	glVertex2f(-0.53799, -3.44154);
	glVertex2f(-0.66258, -3.46947);
	glVertex2f(-0.69265, -3.61983);
	glVertex2f(-0.69937, -3.76975);
	glVertex2f(-0.61131, -3.86581);
	// glVertex2f;
	// glVertex2f;
	// glVertex2f;
	// glVertex2f;
	// glVertex2f;
	// glVertex2f;
	glEnd();

	glBegin(GL_TRIANGLE_FAN);
	glColor3f(0.996, 0.843, 0.69);
	glVertex2f(-1.03205, -2.46846);
	glVertex2f(-0.73991, -2.76275);
	glVertex2f(-0.54092, -2.60575);
	glVertex2f(-0.73034, -2.27768);
	glVertex2f(-0.8, -2.2);
	glEnd();
	glBegin(GL_TRIANGLE_FAN);
	glColor3f(0.95, 0.97, 0.99);
	glVertex2f(-0.83657, -2.84653);
	glVertex2f(-0.73991, -2.76275);
	glVertex2f(-1.03205, -2.46846);
	glVertex2f(-1.28122, -2.82504);
	glVertex2f(-1.28552, -2.91312);
	glVertex2f(-1.0342, -3.16229);
	glVertex2f(-0.66258, -3.46947);
	glVertex2f(-0.53799, -3.44154);
	glVertex2f(-0.48859, -3.3191);
	glEnd();

	glBegin(GL_TRIANGLE_FAN);
	glColor3f(1.0f, 0.94f, 0.87f);//PIEL
	glVertex2f(1.45971, -2.3718);
	glVertex2f(1.43826, -2.07971);
	glVertex2f(1.35496, -2.14913);
	glVertex2f(1.34802, -2.2064);
	glVertex2f(1.23175, -2.31226);
	glVertex2f(1.22828, -2.42159);
	glVertex2f(1.3, -2.5);
	glVertex2f(1.43179, -2.59735);
	glVertex2f(1.73037, -2.35032);
	glVertex2f(1.43826, -2.07971);
	glEnd();



	glBegin(GL_TRIANGLE_FAN);
	glColor3f(0.996, 0.843, 0.69);
	glVertex2f(1.68526, -2.56942);
	glVertex2f(1.73037, -2.35032);
	glVertex2f(1.79481, -2.30521);
	glVertex2f(1.93873, -2.55224);
	glVertex2f(1.6, -2.8);
	glVertex2f(1.43179, -2.59735);
	glVertex2f(1.73037, -2.35032);
	glEnd();

	glBegin(GL_TRIANGLE_FAN);
	glColor3f(0.95, 0.97, 0.99);// blanco
	glVertex2f(1.74326, -2.95393);
	glVertex2f(1.6, -2.8);
	glVertex2f(1.93873, -2.55224);
	glVertex2f(2.29102, -3.10429);
	glVertex2f(1.56497, -3.5425);
	glVertex2f(1.35446, -3.50813);
	glVertex2f(1.3072, -3.36421);
	glEnd();

	glBegin(GL_TRIANGLE_FAN);
	glColor3f(1.0f, 0.94f, 0.87f);//PIEL
	glVertex2f(1.39811, -3.7564);
	glVertex2f(1.39811, -3.91118);
	glVertex2f(1.50752, -3.87382);
	glVertex2f(1.56669, -3.76611);
	glVertex2f(1.5785, -3.68341);
	glVertex2f(1.56497, -3.5425);
	glVertex2f(1.35446, -3.50813);
	glVertex2f(1.31538, -3.70303);
	glEnd();
	glBegin(GL_TRIANGLE_FAN);
	glVertex2f(1.25968, -3.63929);
	glVertex2f(1.31538, -3.70303);
	glVertex2f(1.35446, -3.50813);
	glVertex2f(1.3072, -3.36421);
	glVertex2f(1.25257, -3.40383);
	glVertex2f(1.1363, -3.45937);
	glVertex2f(1.12306, -3.59446);
	glVertex2f(1.15935, -3.67985);
	glEnd();
	
	glBegin(GL_TRIANGLE_FAN); //cuello piel
	glVertex2f(0.26295, -1.82983);
	glVertex2f(0.26673, -2.02279);
	glVertex2f(0.32916, -2.03603);
	glVertex2f(0.56373, -1.91307);
	glVertex2f(0.48685, -1.84509);
	glEnd();
	glBegin(GL_TRIANGLE_FAN); //cuello piel
	glVertex2f(0.76046, -1.80335);
	glVertex2f(0.56373, -1.91307);
	glVertex2f(0.58075, -2.02657);
	glVertex2f(0.8, -2);
	glEnd();

	glBegin(GL_TRIANGLE_FAN);//CARA
	glVertex2f(-0.87843, -0.54295);
	glVertex2f(0.43435, 0.2305);
	glVertex2f(0.48685, -1.84509);//s8
	glVertex2f(0.26295, -1.82983);
	glVertex2f(-0.218, -1.72386);
	glVertex2f(-1.00995, -1.43661);
	glVertex2f(-1.24351, -1.30507);
	glVertex2f(-1.61129, -0.96681);
	glVertex2f(-1.63545, -0.41648);
	glVertex2f(-1.64,0);
	glVertex2f(-1.57908, 0.31372);
	glVertex2f(-1.49854, 0.67077);
	glVertex2f(-1.24351, 0.71641);
	glVertex2f(-1.07975, 1.12178);
	glVertex2f(-0.89183, 1.47614);
	glVertex2f(-0.89183, 1.47614);
	glVertex2f(-0.71733, 1.72312);
	glVertex2f(-0.48646, 2.01574);
	glVertex2f(-0.31733, 2.18487);
	glVertex2f(-0.30659, 1.71775);
	glVertex2f(-0.24753, 1.50836);
	glVertex2f(0, 0.8);
	glVertex2f(0.15515, 0.53386);
	glVertex2f(0.43435, 0.2305);
	glEnd();

	glBegin(GL_TRIANGLE_FAN);//ojo I
	
	glColor3f(0.95, 0.97, 0.99);
	glVertex2f(-0.68236, 0.22487);
	glVertex2f(-0.70622, 0.79774);
	glVertex2f(-0.47824, 0.7795);
	glVertex2f(-0.35665, 0.71871);
	glVertex2f(-0.2, 0.6);
	glVertex2f(-0.11043, 0.48465);
	glVertex2f(-0.06484, 0.29922);
	glVertex2f(-0.0618, 0.08036);
	glVertex2f(-0.11955, -0.11115);
	glVertex2f(-0.21682, -0.27529);
	glVertex2f(-0.34753, -0.36649);
	glVertex2f(-0.5208, -0.41512);
	glVertex2f(-0.67887, -0.4212);
	glVertex2f(-0.85821, -0.37864);
	glVertex2f(-1.01932, -0.29961);
	glVertex2f(-1.15307, -0.17802);
	glVertex2f(-1.2777, -0.06251);
	glVertex2f(-1.38713, 0.06212);
	glVertex2f(-1.361, 0.11695);
	glVertex2f(-1.44488, 0.27794);
	glVertex2f(-1.34153, 0.42993);
	glVertex2f(-1.24426, 0.55152);
	glVertex2f(-1.08011, 0.69439);
	glVertex2f(-0.88861, 0.77646);
	glVertex2f(-0.70622, 0.79774);
	glEnd();
	
	glBegin(GL_TRIANGLE_FAN);//
	glColor3f(0.61f, 0.82f, 0.94f);
	glVertex2f(-0.68236, 0.22487);
	glVertex2f(-0.27,0);
	glVertex2f(-0.32014, -0.09945);
	glVertex2f(-0.4043, -0.2257);
	glVertex2f(-0.53056, -0.3125);
	glVertex2f(-0.8646, -0.27831);
	glVertex2f(-0.94614, -0.22307);
	glVertex2f(-1.05135, -0.11523);
	glVertex2f(-1.14078, 0.03206);
	glVertex2f(-1.13551, 0.48446);
	glVertex2f(-1.0382, 0.5923);
	glVertex2f(-0.94088, 0.70804);
	glVertex2f(-0.70622, 0.79774);
	glVertex2f(-0.47824, 0.7795);
	glVertex2f(-0.41219, 0.72119);
	glVertex2f(-0.3254, 0.64228);
	glVertex2f(-0.26227, 0.51866);
	glVertex2f(-0.27,0);
	glEnd();

	glBegin(GL_TRIANGLE_FAN);//
	glColor3f(1.0f, 0.94f, 0.87f);
	glVertex2f(-1.49854, 0.67077);
	glVertex2f(-1.57908, 0.31372);
	glVertex2f(-1.24351, 0.71641);
	glVertex2f(-1.30525, 1.02513);
	glVertex2f(-1.46901, 1.00903);
	glEnd();

	glBegin(GL_TRIANGLE_FAN);// 
	glColor3f(1.0f, 0.94f, 0.87f);//PIEL
	glVertex2f(1.48761, -1.21165);
	glVertex2f(0.43435, 0.2305);
	glVertex2f(0.34039, 0.56876);
	glVertex2f(0.45582, 0.59023);
	glVertex2f(0.59273, 0.35131);
	glVertex2f(0.83971, 0.10164);
	glVertex2f(1.11086, -0.03258);
	glVertex2f(1.05716, 0.24124);
	glVertex2f(1.01421, 0.5607);
	glVertex2f(0.97394, 0.9795);
	glVertex2f(0.96857, 1.45735);
	glVertex2f(1.06522, 2.26809);
	glVertex2f(1.30683, 1.79292);
	glVertex2f(1.53233, 1.48419);
	glVertex2f(1.6773, 1.29628);
	glVertex2f(1.96455, 0.99829);
	glVertex2f(2.24374, 0.79963);
	glVertex2f(2.47193, 0.64661);
	glVertex2f(2.66253, 0.58487);
	glVertex2f(2.62495, 0.16607);
	glVertex2f(2.5471, -0.21245);
	glVertex2f(2.48804, -0.52654);
	glVertex2f(2.42361, -0.89701);
	glVertex2f(2.3028, -1.26748);
	glVertex2f(1.92696, -1.46077);
	glVertex2f(1.26924, -1.67822);
	glVertex2f(0.87964, -1.80335);
	glVertex2f(0.76046, -1.80335);
	glVertex2f(0.48685, -1.84509);
	glVertex2f(0.26295, -1.82983);
	glVertex2f(0.43435, 0.2305);
	glEnd();
	glBegin(GL_TRIANGLES);//
	glVertex2f(0.3028, 0.89091);
	glVertex2f(0.34039, 0.56876);
	glVertex2f(0.45582, 0.59023);
	glEnd();

	glBegin(GL_TRIANGLE_FAN);//ojo D
	glColor3f(0.95, 0.97, 0.99);
	glVertex2f(1.73418, 0.17874);
	glVertex2f(1.70649, 0.75231);
	glVertex2f(2.29984, 0.4774);
	glVertex2f(2.43829, 0.28752);
	glVertex2f(2.42642, 0.18468);
	glVertex2f(2.33148, 0.03436);
	glVertex2f(2.36313, -0.00915);
	glVertex2f(2.26226, -0.14364);
	glVertex2f(2.13964, -0.29396);
	glVertex2f(1.89541, -0.44952);
	glVertex2f(1.49882, -0.4423);
	glVertex2f(1.3378, -0.35618);
	glVertex2f(1.21796, -0.22276);
	glVertex2f(1.11086, -0.03258);
	glVertex2f(1.05716, 0.24124);
	glVertex2f(1.09732, 0.42597);
	glVertex2f(1.34257, 0.61782);
	glVertex2f(1.5542, 0.72858);
	glVertex2f(1.70649, 0.75231);
	glVertex2f(1.89636, 0.74638);
	glVertex2f(2.06448, 0.66925);
	glEnd();
	glBegin(GL_TRIANGLE_FAN);//
	glColor3f(0.61f, 0.82f, 0.94f);//0.61f, 0.82f, 0.94f
	glVertex2f(1.73418, 0.17874);
	glVertex2f(2.1467, 0.42399);
	glVertex2f(2.07969, 0.5963);
	glVertex2f(2.06448, 0.66925);
	glVertex2f(1.89636, 0.74638);
	glVertex2f(1.70649, 0.75231);
	glVertex2f(1.5542, 0.72858);
	glVertex2f(1.46703, 0.64177);
	glVertex2f(1.39284, 0.5628);
	glVertex2f(1.29951, -0.04268);
	glVertex2f(1.4, -0.2);
	glVertex2f(1.49575, -0.30832);
	glVertex2f(1.60584, -0.349);
	glVertex2f(1.80447, -0.36336);
	glVertex2f(1.96003, -0.3155);
	glVertex2f(2.05815, -0.18866);
	glVertex2f(2.11798, -0.10968);
	glVertex2f(2.1467, 0.42399);
	glEnd();
	
	glBegin(GL_TRIANGLE_FAN);//boca
	glColor3f(0.925, 0.486, 0.517);
	glVertex2f(0.31945, -1.23098);
	glVertex2f(0.56102, -1.22462);
	glVertex2f(0.77715, -1.26912);
	glVertex2f(0.86191, -1.30726);
	glVertex2f(0.76656, -1.36659);
	glVertex2f(0.58673, -1.43665);
	glVertex2f(0.43185, -1.43665);
	glVertex2f(0.31385, -1.42559);
	glVertex2f(0.2, -1.4);
	glVertex2f(0.11103, -1.34078);
	glVertex2f(0.04466, -1.29284);
	glVertex2f(0.14146, -1.25217);
	glEnd();

       	glBegin(GL_TRIANGLE_FAN);//
	glColor3f(0.780, 0.2, 0.19);
	glVertex2f(0.77715, -1.26912);
	glVertex2f(0.56102, -1.22462);
	glVertex2f(0.48717, -1.05683);
	glVertex2f(0.8338, -1.09002);
	glVertex2f(0.95515, -1.12715);
	glVertex2f(0.92336, -1.24369);
	glVertex2f(0.86191, -1.30726);
	glEnd();
	glBegin(GL_TRIANGLE_FAN);//
	glVertex2f(0.19954, -1.05314);
	glVertex2f(0.48717, -1.05683);
	glVertex2f(0.56102, -1.22462);
	glVertex2f(0.31945, -1.23098);
	glVertex2f(0.14146, -1.25217);
	glVertex2f(0.04466, -1.29284);
	glVertex2f(-0.02541, -1.23384);
	glVertex2f(-0.04753, -1.06789);
	// glVertex2f;
	glEnd();

	glBegin(GL_TRIANGLE_FAN);//bolas cabeza
	glColor3f(0.996, 0.843, 0.69);// amarillo i
        glVertex2f(3.39855, 3.29842);
	glVertex2f(3.159, 3.4599);
	glVertex2f(3.11267, 3.29506);
	glVertex2f(3.32792, 3.02599);
        glVertex2f(3.53981, 3.02936);
	glVertex2f(3.6788, 3.2069);
	glVertex2f(3.6834, 3.4001);
	glVertex2f(3.5224, 3.5703);
        glVertex2f(3.3292, 3.6163);
	glVertex2f(3.159, 3.4599);
	// glVertex2f;
	// glVertex2f;
	glEnd();
	glBegin(GL_TRIANGLE_FAN);//bolas cabeza
	glColor3f(0.996, 0.843, 0.69);// amarillo i
	glVertex2f(2.37275, 4.37131);
	glVertex2f(2.61161, 4.52249);
	glVertex2f(2.54261, 4.58688);
	glVertex2f(2.33561, 4.56388);
	glVertex2f(2.19761, 4.45349);
	glVertex2f(2.16541, 4.26949);
	glVertex2f(2.25281, 4.14529);
	glVertex2f(2.44141, 4.10389);
	glVertex2f(2.50581, 4.26029);
	glVertex2f(2.61161, 4.52249);
	glEnd();

	glBegin(GL_TRIANGLE_FAN);//
	glColor3f(0.309803921569, 0.372549019608, 0.611764705882);// Azul obcuro ropa
	glVertex2f(3.2, 4.2);
	glVertex2f(3.4, 4.8);
	glVertex2f(3.6558, 4.61448);
	glVertex2f(3.8, 4.4);
	glVertex2f(3.94559, 4.16829);
	glVertex2f(3.94099, 3.93369);
	glVertex2f(3.89499, 3.71289);
	glVertex2f(3.78869, 3.54394);
	glVertex2f(3.65416, 3.48004);
	glVertex2f(3.5224, 3.5703);
	glVertex2f(3.3292, 3.6163);
	glVertex2f(3.159, 3.4599);
	glVertex2f(3.11267, 3.29506);
	glVertex2f(2.82331, 3.59188);
	glVertex2f(2.4871, 3.88607);
	glVertex2f(2.40921, 3.92909);
	glVertex2f(2.44141, 4.10389);
	glVertex2f(2.50581, 4.26029);
	glVertex2f(2.61161, 4.52249);
	glVertex2f(2.8324, 4.70648);
	glVertex2f(3.0992, 4.80768);
	glVertex2f(3.4, 4.8);
	glEnd();

	glBegin(GL_TRIANGLE_FAN);//
	glColor3f(0.274509803922, 0.780, 0.866);// Azul obcuro cabello
	glVertex2f(3.60499, 5.08461);
	glVertex2f(3.4, 4.8);
	glVertex2f(3.0992, 4.80768);
	glVertex2f(3.0992, 5.03308);
	glVertex2f(3.0486, 5.24468);
	glVertex2f(2.883, 5.44248);
	glVertex2f(1.49382, 6.27967);
	glVertex2f(1.85262, 6.38087);
	glVertex2f(2.32181, 6.41306);
	glVertex2f(2.80021, 6.30727);
	glVertex2f(3.2142, 6.16007);
	glVertex2f(3.4856, 5.90707);
	glVertex2f(3.74779, 5.57127);
	glVertex2f(3.86739, 5.17568);
	glVertex2f(3.90419, 4.70648);
	glVertex2f(3.90419, 4.70648);
	glVertex2f(3.8, 4.4);
	glVertex2f(3.6558, 4.61448);
	glVertex2f(3.4, 4.8);
	glEnd();
	glBegin(GL_TRIANGLE_FAN);//
	glColor3f(0.274509803922, 0.780, 0.866);// Azul obcuro cabello
	glVertex2f(1.18925, 5.53281);
	glVertex2f(1.49382, 6.27967);
	glVertex2f(2.883, 5.44248);
	glVertex2f(2.60701, 5.49767);
	glVertex2f(2.27581, 5.39648);
	glVertex2f(1.94921, 5.17568);
	glVertex2f(1.72842, 4.91348);
	glVertex2f(1.65022, 4.65128);
	glVertex2f(1.66402, 4.29709);
	glVertex2f(1.31875, 4.38198);
	glVertex2f(0.70516, 4.51647);
	glVertex2f(0.47723, 4.47189);
	glVertex2f(0.34843, 4.84908);
	glVertex2f(0.35763, 5.24468);
	glVertex2f(0.58763, 5.63567);
	glVertex2f(0.88203, 5.90247);
	glVertex2f(1.18102, 6.07727);
	glVertex2f(1.49382, 6.27967);
	glEnd();
	glBegin(GL_TRIANGLE_FAN);//
	glColor3f(0.274509803922, 0.780, 0.866);// Azul obcuro cabello
	glVertex2f(1.18925, 5.53281);
	glVertex2f(1.49382, 6.27967);
	glVertex2f(2.883, 5.44248);
	glVertex2f(2.60701, 5.49767);
	glVertex2f(2.27581, 5.39648);
	glVertex2f(1.94921, 5.17568);
	glVertex2f(1.72842, 4.91348);
	glVertex2f(1.65022, 4.65128);
	glVertex2f(1.66402, 4.29709);
	glVertex2f(1.31875, 4.38198);
	glVertex2f(0.70516, 4.51647);
	glVertex2f(0.47723, 4.47189);
	glVertex2f(0.34843, 4.84908);
	glVertex2f(0.35763, 5.24468);
	glVertex2f(0.58763, 5.63567);
	glVertex2f(0.88203, 5.90247);
	glVertex2f(1.18102, 6.07727);
	glVertex2f(1.49382, 6.27967);
	glEnd();

	glBegin(GL_TRIANGLE_FAN);//
	glVertex2f(4.14799, 3.3679);
	glVertex2f(4.4, 3.6);
	glVertex2f(4.61259, 4.13609);
	glVertex2f(4.41479, 3.87849);
	glVertex2f(4.22619, 3.75889);
	glVertex2f(3.78869, 3.54394);
	glVertex2f(3.78869, 3.54394);
	glVertex2f(3.65416, 3.48004);
	glVertex2f(3.6834, 3.4001);
	glVertex2f(3.6788, 3.2069);
	glVertex2f(3.53981, 3.02936);
	glVertex2f(3.43691, 2.76815);
	glVertex2f(3.43691, 2.76815);
	glVertex2f(3.5408, 2.62731);
	glVertex2f(3.81679, 2.53071);
	glVertex2f(4.23539, 2.51691);
	glVertex2f(4.43319, 2.62731);
	glVertex2f(4.71838, 2.8435);
	glVertex2f(4.93458, 3.1425);
	glVertex2f(4.38259, 3.3449);
	glEnd();
	glBegin(GL_TRIANGLE_FAN);//
	glVertex2f(5.08638, 3.5887);
	glVertex2f(4.93458, 3.1425);
	glVertex2f(4.38259, 3.3449);
	glVertex2f(4.65399, 3.4001);

	glVertex2f(4.92998, 3.6025);
	glVertex2f(5.11858, 3.90609);
	glEnd();
	glBegin(GL_TRIANGLE_FAN);//
	glVertex2f(4.4, 3.6);
	glVertex2f(4.61259, 4.13609);
	glVertex2f(4.41479, 3.87849);
	glVertex2f(4.22619, 3.75889);
	glVertex2f(3.89499, 3.71289);
	glVertex2f(3.78869, 3.54394);
	glVertex2f(4.14799, 3.3679);
	glEnd();
	glBegin(GL_TRIANGLES);//
	glVertex2f(3.53981, 3.02936);
	glVertex2f(3.32792, 3.02599);
	glVertex2f(3.43691, 2.76815);
	
	glVertex2f(4.06059, 0.23073);
	glVertex2f(3.7202, 0.38253);
	glVertex2f(3.6926, 0.21693);
	
	glVertex2f(-3, 0.2);
	glVertex2f(-2.86693, -0.37186);
	glVertex2f(-3, 0.2);
	
	glVertex2f(-1.24351, 0.71641);
	glVertex2f(-1.30525, 1.02513);
	glVertex2f(-1.07975, 1.12178);
	
	// glVertex2f;
	// glVertex2f;
	// glVertex2f;
	glEnd();

	

	
	glBegin(GL_TRIANGLE_FAN);//
	glVertex2f(0.5585, 2.56271);
	glVertex2f(1.06522, 2.26809);
	glVertex2f(0.96857, 1.45735);
	glVertex2f(0.97394, 0.9795);
	glVertex2f(1.01421, 0.5607);
	glVertex2f(1.05716, 0.24124);
	glVertex2f(1.11086, -0.03258);
	glVertex2f(0.83971, 0.10164);
	glVertex2f(0.59273, 0.35131);
	glVertex2f(0.45582, 0.59023);
	glVertex2f(0.3028, 0.89091);
	glVertex2f(0.15515, 0.53386);
	glVertex2f(0, 0.8);
	glVertex2f(-0.08646, 1.0305);
	glVertex2f(-0.19384, 1.28822);
	glVertex2f(-0.24753, 1.50836);
	glVertex2f(-0.30659, 1.71775);
	glVertex2f(-0.31733, 2.18487);
	glVertex2f(-2.96353, 1.57852);
	glVertex2f(-2.91755, 1.87718);
	glVertex2f(-2.77466, 2.24702);
	glVertex2f(-2.55612, 2.64207);
	glVertex2f(-2.42164, 2.91104);
	glVertex2f(-2.10223, 3.3145);
	glVertex2f(-1.63994, 3.71796);
	glVertex2f(-1.28691, 4.01215);
	glVertex2f(-0.8078, 4.19707);
	glVertex2f(-0.30348, 4.39039);
	glVertex2f(0.21765, 4.44923);
	glVertex2f(0.47723, 4.47189);
	glVertex2f(0.47723, 4.47189);
	glVertex2f(1.31875, 4.38198);
	glVertex2f(1.66402, 4.29709);
	glVertex2f(1.92394, 4.19707);
	glVertex2f(2.25175, 4.01215);
	glVertex2f(2.40921, 3.92909);
	glVertex2f(2.4871, 3.88607);
	glVertex2f(2.82331, 3.59188);
	glVertex2f(3.11267, 3.29506);
	glVertex2f(3.32792, 3.02599);
	glVertex2f(3.5408, 2.62731);
	glVertex2f(3.68907, 2.28064);
	glVertex2f(3.76472, 1.95283);
	glVertex2f(3.80674, 1.65864);
	glVertex2f(3.81515, 1.34765);
	glVertex2f(3.82355, 1.08708);
	glVertex2f(3.83059, 0.98972);
	glVertex2f(4.08819, 0.85632);
	glVertex2f(3.91799, 0.83332);
	glVertex2f(3.80759, 0.89312);
	glVertex2f(1.06522, 2.26809);
	glEnd();
	glBegin(GL_TRIANGLE_FAN);//
	glVertex2f(3.44433, 0.79005);
	glVertex2f(3.80759, 0.89312);
	glVertex2f(1.06522, 2.26809);
	glVertex2f(1.30683, 1.79292);
	glVertex2f(1.53233, 1.48419);
	glVertex2f(1.6773, 1.29628);
	glVertex2f(1.96455, 0.99829);
	glVertex2f(2.24374, 0.79963);
	glVertex2f(2.47193, 0.64661);
	glVertex2f(2.66253, 0.58487);
	glVertex2f(2.62495, 0.16607);
	glVertex2f(2.5471, -0.21245);
	glVertex2f(2.48804, -0.52654);
	glVertex2f(2.42361, -0.89701);
	glVertex2f(2.3028, -1.26748);
	glVertex2f(2.26201, -1.49885);
	glVertex2f(2.14241, -1.86225);
	glVertex2f(2, -2.2);
	glVertex2f(2.40921, -1.90365);
	glVertex2f(2.68521, -1.59085);
	glVertex2f(2.8922, -1.31945);
	glVertex2f(3.067, -1.08025);
	glVertex2f(3.2464, -0.78586);
	glVertex2f(3.3798, -0.51446);
	glVertex2f(3.5362, -0.21546);
	glVertex2f(3.6, 0);
	glVertex2f(3.6926, 0.21693);
	glVertex2f(3.7202, 0.38253);
	glVertex2f(3.76619, 0.59873);
	glVertex2f(3.80759, 0.89312);
	glVertex2f(1.92394, 4.19707);
	glEnd();
	glBegin(GL_TRIANGLE_FAN);//
	glVertex2f(-2.74274, 0.69073);
	glVertex2f(-2.87153, 0.18473);
	glVertex2f(-3, 0.2);
	glVertex2f(-3.01873, 0.92072);
	glVertex2f(-2.96353, 1.57852);
	glVertex2f(-0.31733, 2.18487);
	glVertex2f(-0.48646, 2.01574);
	glVertex2f(-0.71733, 1.72312);
	glVertex2f(-1.34015, 1.35265);
	glVertex2f(-1.46901, 1.00903);
	glVertex2f(-1.49854, 0.67077);
	glVertex2f(-1.57908, 0.31372);
	glVertex2f(-1.63639, -0.00357);
	glVertex2f(-1.63545, -0.41648);
	glVertex2f(-1.61129, -0.96681);
	glVertex2f(-2.68294, 0.02373);
	glBegin(GL_TRIANGLE_FAN);//
	glVertex2f(-1.34015, 1.35265);
	glVertex2f(-0.71733, 1.72312);
	glVertex2f(-0.89183, 1.47614);
	glVertex2f(-1.07975, 1.12178);
	glVertex2f(-1.30525, 1.02513);
	glEnd();
	glBegin(GL_TRIANGLE_FAN);//
	glVertex2f(0.15515, 0.53386);
	glVertex2f(0.43435, 0.2305);
	glVertex2f(0.34039, 0.56876);
	glVertex2f(0.3028, 0.89091);
	glVertex2f(0.5585, 2.56271);
	// glVertex2f;
	// glVertex2f;
	glEnd();
	glBegin(GL_TRIANGLE_FAN);//
	glVertex2f(-1.64679, -2.07044);
	glVertex2f(-2.66914, -0.44086);
	glVertex2f(-1.61129, -0.96681);
	glVertex2f(-1.24351, -1.30507);
	glVertex2f(-1.00995, -1.43661);
	glVertex2f(0.04073, -1.82193);
	glVertex2f(-0.3716, -1.97993);
	glVertex2f(-0.48996, -1.94747);
	glVertex2f(-0.8, -2.2);
	// glVertex2f;
	glVertex2f(-1.03205, -2.46846);
	glVertex2f(-1.28552, -2.91312);
	glVertex2f(-1.28552, -2.91312);
	glVertex2f(-0.89684, -3.91918);
	glVertex2f(-1.03292, -4.17322);
	glVertex2f(-1.29375, -4.91661);
	glVertex2f(-2.00674, -4.96261);
	glVertex2f(-2.01594, -4.23122);
	// glVertex2f;
	glVertex2f(-2.22294, -4.23122);
	glVertex2f(-2.75194, -4.42902);
	glVertex2f(-3.27633, -4.55322);
	glVertex2f(-3.80992, -4.56702);
	glVertex2f(-3.71792, -4.18522);
	glVertex2f(-3.33153, -3.74363);
	glVertex2f(-3.04173, -3.27903);
	glVertex2f(-3.54773, -3.52283);
	glVertex2f(-4.32512, -3.62403);
	glVertex2f(-4.11352, -3.41243);
	glVertex2f(-3.66273, -3.12723);
	glVertex2f(-3.33613, -2.81904);
	glVertex2f(-3.10153, -2.41424);
	glVertex2f(-2.85773, -1.88985);
	glVertex2f(-2.71974, -1.37465);
	glVertex2f(-2.66914, -0.44086);
	glVertex2f(-2.74274, 0.69073);
	glVertex2f(-1.63639, -0.00357);
	glEnd();


	glBegin(GL_TRIANGLE_FAN);//
	glVertex2f(-0.73991, -2.76275);
	glVertex2f(-0.54092, -2.60575);
	glVertex2f(-0.45369, -2.61752);
	glVertex2f(-0.4785, -2.73778);
	glVertex2f(-0.45559, -2.89241);
	glVertex2f(-0.4, -3);
	glVertex2f(-0.31242, -3.1253);
	glVertex2f(-0.33915, -3.3181);
	glVertex2f(-0.48859, -3.3191);
	glVertex2f(-0.83657, -2.84653);
	glEnd();
	glBegin(GL_TRIANGLE_FAN);//
	glVertex2f(-1.0342, -3.16229);
	glVertex2f(-1.28552, -2.91312);
	glVertex2f(-0.89684, -3.91918);
	glVertex2f(-0.69937, -3.76975);
	glVertex2f(-0.69265, -3.61983);
	glVertex2f(-0.66258, -3.46947);
	glEnd();

	glBegin(GL_TRIANGLE_FAN);//
	glVertex2f(-2.39774, -4.48422);
	glVertex2f(-2.63694, -4.70962);
	glVertex2f(-2.32874, -4.56242);
	glVertex2f(-2.01594, -4.23122);
	glVertex2f(-2.22294, -4.23122);
	glEnd();
	glBegin(GL_TRIANGLE_FAN);//
	glVertex2f(-1.29375, -4.91661);
	glVertex2f(-0.83376, -4.75562);
	glVertex2f(-0.64056, -4.62682);
	glVertex2f(-0.6521, -4.42287);
	glVertex2f(-1.03292, -4.17322);
	glVertex2f(-2.01594, -4.23122);
	glVertex2f(-2.14934, -4.56242);
	glVertex2f(-2.4, -4.8);
	glVertex2f(-2.00674, -4.96261);
	glEnd();
	
	glBegin(GL_TRIANGLE_FAN);//
	glVertex2f(1.52764, -2.8928);
	glVertex2f(1.3, -2.5);
	glVertex2f(1.29075, -2.63852);
	glVertex2f(1.32737, -2.70724);
	glVertex2f(1.34073, -2.7855);
	glVertex2f(1.32025, -2.87627);
	glVertex2f(1.29769, -2.96477);
	glVertex2f(1.22133, -3.06543);
	glVertex2f(1.1658, -3.15393);
	glVertex2f(1.1, -3.2);
	glVertex2f(1.17621, -3.23897);
	glVertex2f(1.25257, -3.40383);
	glVertex2f(1.74326, -2.95393);
	glVertex2f(1.6, -2.8);
	glVertex2f(1.43179, -2.59735);
	glVertex2f(1.3, -2.5);
	glEnd();

	glBegin(GL_TRIANGLE_FAN);//
	glVertex2f(1.45735, -1.99468);
	glVertex2f(1.29943, -1.97906);
	glVertex2f(1.12556, -1.96225);
	glVertex2f(0.87964, -1.80335);
	glVertex2f(1.26924, -1.67822);
	glVertex2f(1.92696, -1.46077);
	glVertex2f(2.3028, -1.26748);
	glVertex2f(2.26201, -1.49885);
	glVertex2f(2.14241, -1.86225);
	glVertex2f(2, -2.2);
	glVertex2f(1.79481, -2.30521);
	glVertex2f(1.73037, -2.35032);
	glVertex2f(1.43826, -2.07971);
	glEnd();
	glBegin(GL_TRIANGLE_FAN);//
	glVertex2f(3.45904, -2.85204);
	//	glVertex2f(-1.44488, 0.27794);
	glVertex2f(3.5362, -0.21546);
	glVertex2f(3.3798, -0.51446);
	glVertex2f(3.2464, -0.78586);
	glVertex2f(3.067, -1.08025);
	glVertex2f(2.8922, -1.31945);
	glVertex2f(2.68521, -1.59085);
	glVertex2f(2.40921, -1.90365);
	glVertex2f(2, -2.2);
	glVertex2f(1.73037, -2.35032);
	glVertex2f(1.93873, -2.55224);
	glVertex2f(2.29102, -3.10429);
	glVertex2f(1.56497, -3.5425);
	glVertex2f(1.5785, -3.68341);
	glVertex2f(1.50752, -3.87382);
	glVertex2f(1.50752, -3.87382);
	glVertex2f(1.6, -4);
	glVertex2f(1.63285, -4.10129);
	glVertex2f(1.71748, -4.16899);
	glVertex2f(1.44667, -4.45249);
	glVertex2f(1.23622, -4.84301);
	glVertex2f(1.83882, -4.41062);
	glVertex2f(2.15621, -4.84761);
	glVertex2f(2.11021, -5.03621);
	glVertex2f(2.40461, -4.83381);
	glVertex2f(2.67601, -4.40142);
	glVertex2f(3.0394, -4.67742);
	glVertex2f(3.3154, -4.83841);
	glVertex2f(3.75239, -4.96261);
	glVertex2f(3.83866, -4.63183);
	glVertex2f(3.96859, -3.90002);
	glVertex2f(4.45159, -4.06102);
	glVertex2f(4.79198, -4.09322);
	glVertex2f(5.14158, -3.91382);
	glVertex2f(4.87478, -3.47223);
	glVertex2f(4.47459, -3.21923);
	glVertex2f(4.11119, -2.80064);
	glVertex2f(3.79839, -2.04164);
	glVertex2f(3.619, -1.50345);
	glVertex2f(3.6, 0);
	glVertex2f(3.45904, -2.85204);
	glVertex2f(2.62495, 0.16607);
	glEnd();

	glBegin(GL_TRIANGLE_FAN);//
	glColor3f(0.f, 0.f, 0.192f);//cejas
	glVertex2f(1.3, 0.7);
	glVertex2f(1.08948, 0.46274);
	glVertex2f(1.09732, 0.42597);
	glVertex2f(1.34257, 0.61782);
	glVertex2f(1.5542, 0.72858);
	glVertex2f(1.70649, 0.75231);
	glVertex2f(1.94701, 0.80067);
	glVertex2f(1.62907, 0.81702);
	glVertex2f(1.94701, 0.80067);
	// glVertex2f;
	// glVertex2f;
	// glVertex2f;
	// glVertex2f;
	glEnd();

	glBegin(GL_TRIANGLE_FAN);//
	glVertex2f(1.62907, 0.81702);
	glVertex2f(1.94701, 0.80067);
	glVertex2f(2.17047, 0.74071);
	glVertex2f(2.02225, 0.67767);
	glVertex2f(1.89636, 0.74638);
	glVertex2f(1.70649, 0.75231);
	glEnd();
	
	glBegin(GL_TRIANGLE_FAN);//

	glVertex2f(2.35215, 0.58992);
	glVertex2f(2.17047, 0.74071);
	glVertex2f(1.89636, 0.74638);
	glVertex2f(1.89636, 0.74638);
	glVertex2f(1.89636, 0.74638);
	glVertex2f(2.06448, 0.66925);
	glVertex2f(2.29984, 0.4774);
	glVertex2f(2.43829, 0.28752);
	glVertex2f(2.60469, 0.23201);
	glEnd();
	glBegin(GL_TRIANGLE_FAN);//
	glVertex2f(1.94701, 0.80067);
	glVertex2f(1.89636, 0.74638);
	glVertex2f(1.70649, 0.75231);
	glVertex2f(1.62907, 0.81702);
	glEnd();
	glBegin(GL_TRIANGLE_FAN);//
	glVertex2f(2.17047, 0.74071);
	glVertex2f(2.06448, 0.66925);
	glVertex2f(1.89636, 0.74638);
	glVertex2f(2.17047, 0.74071);
	glEnd();
	glBegin(GL_TRIANGLE_FAN);//
	glVertex2f(2.60469, 0.23201);
	glVertex2f(2.35215, 0.58992);
	glVertex2f(2.29984, 0.4774);
	glVertex2f(2.43829, 0.28752);
	glVertex2f(2.42642, 0.18468);
	glVertex2f(2.33148, 0.03436);
	glVertex2f(2.36313, -0.00915);
	// glVertex2f;
	glEnd();

	glBegin(GL_TRIANGLE_FAN);//
	glVertex2f(-1.57908, 0.31372);
	glVertex2f(-1.38713, 0.06212);
	glVertex2f(-1.361, 0.11695);
	glVertex2f(-1.44488, 0.27794);
	glVertex2f(-1.34153, 0.42993);
	glVertex2f(-1.24426, 0.55152);
	glVertex2f(-1.08011, 0.69439);
	glVertex2f(-1.24351, 0.71641);
	glEnd();

	glBegin(GL_TRIANGLE_FAN);//
	glVertex2f(-0.93079, 0.87152);
	glVertex2f(-1.24351, 0.71641);
	glVertex2f(-1.24426, 0.55152);
	glVertex2f(-1.08011, 0.69439);
	glVertex2f(-0.88861, 0.77646);
	glVertex2f(-0.70622, 0.79774);
	glVertex2f(-0.47824, 0.7795);
	glVertex2f(-0.18409, 0.73526);
	glVertex2f(-0.43663, 0.87697);
	// glVertex2f;
	// glVertex2f;
	// glVertex2f;
	glEnd();
	glBegin(GL_TRIANGLE_FAN);//
	glVertex2f(-0.18409, 0.73526);
	glVertex2f(-0.47824, 0.7795);
	glVertex2f(-0.35665, 0.71871);
	glVertex2f(-0.2, 0.6);
	glVertex2f(-0.08235, 0.5445);
	glEnd();

	// glBegin(GL_TRIANGLE_FAN);//
	// glColor3f(0.61f, 0.82f, 0.94f);//
	// glVertex2f(-0.68236, 0.22487);
	// glVertex2f(-0.8127, 0.42822);
	// glVertex2f(-0.72368, 0.53905);
	// glVertex2f(-0.61104, 0.54087);
	// glVertex2f(-0.46206, 0.39734);
	// glVertex2f(-0.46206, 0.12482);
	// glVertex2f(-0.5529, -0.01507);
	// glVertex2f(-0.68916, -0.01689);
	// glVertex2f(-0.8, 0.1);
	// glVertex2f(-0.8127, 0.42822);
	// glEnd();

	
	
	// glBegin(GL_TRIANGLE_FAN);//
	// glColor3f(1.0f, 0.94f, 0.87f);//PIEL
	// glVertex2f;
	// glVertex2f;
	// glVertex2f;
	// glVertex2f;
	// glEnd();

	glEnd();
	glFlush();
}


void Initialize() {
	glClearColor(0.57f, 0.81f, 0.89f,0);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-10.0, 10.0, -10.0, 10.0, -10.0, 10.0);
}
int main(int iArgc, char** cppArgv) {
	glutInit(&iArgc, cppArgv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(476, 477);
	glutInitWindowPosition(250, 250);
	glutCreateWindow("Aqua Konosuba");
	Initialize();
	glutDisplayFunc(Draw);
	glutMainLoop();
	return EXIT_SUCCESS;
}
