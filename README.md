Dibujo 2D
---

Alumno:  Romero Andrade Cristian

---

* Personaje

Diosa Aqua de Konosuba

Resultado en openGJ:


![Diosa Inutil](Diosa_Aqua.png "Aqua de Konosuba")

* Compilación:

Este binario fue generado en: 

* Archlinux 
* kernel 5.5.7
* g++: 9.2.1 20200130

## Comando de compilación

```bash
g++ aqua.cxx -lglut -lGLU -lGL -o aqua.out
```

Ejecutar ejecutando la instrucción: `./aqua.out`

[Link Del repositorio][bb]

[bb]: https://bitbucket.org/CrisRomAnd/diosa-inutil/src/master/
